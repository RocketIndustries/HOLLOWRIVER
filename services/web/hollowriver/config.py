import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "postgres://")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.getenv("SECRET_KEY")
    APP_UPLOADS_FOLDER = os.getenv("APP_UPLOADS_FOLDER")
    APP_DOWNLOADS_FOLDER = os.getenv("APP_DOWNLOADS_FOLDER")
