from . import db

from flask_login import UserMixin


class Users(UserMixin, db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(165))

    def __repr__(self):
        return self.username


class Hosts(db.Model):
    __tablename__ = "hosts"
    id = db.Column(db.Integer, primary_key=True)
    hostname = db.Column(db.String(100), nullable=False)
    date = db.Column(db.String(100))
    os = db.Column(db.String(100))
    architecture = db.Column(db.String(3))
    account = db.Column(db.String(100))
    external_ip = db.Column(db.String(16))

    def __repr__(self):
        return self.hostname


class Tasks(db.Model):
    __tablename__ = "tasks"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), nullable=False)
    hostname = db.Column(db.String(100), nullable=False)
    action = db.Column(db.String(20))
    secondary = db.Column(db.String(65535))

    def __repr__(self):
        return self.username


class Output(db.Model):
    __tablename__ = "output"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), nullable=False)
    hostname = db.Column(db.String(100), nullable=False)
    action = db.Column(db.String(20))
    secondary = db.Column(db.String(65535))
    stdout = db.Column(db.String(65535))
    stderr = db.Column(db.String(65535))
    status = db.Column(db.String(16))

    def __repr__(self):
        return self.username
