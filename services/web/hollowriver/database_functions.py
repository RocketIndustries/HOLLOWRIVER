import psycopg2
from psycopg2 import extras
import json, base64, os

conn = {"database": os.getenv("POSTGRES_DB"),
        "username": os.getenv("POSTGRES_USER"),
        "password": os.getenv("POSTGRES_PASSWORD"),
        "host": "database",
        "port": 5432}


def beacon(data):
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("SELECT hostname from hosts where hostname = %s", (data['hostname'],))
    res = cur.fetchone()
    if res:
        """Update the host timestamp in the database"""
        sql = """ UPDATE hosts SET date = %s, external_ip = %s WHERE hostname = %s """
        cur.execute(sql, (data["date"], data["external_ip"], data["hostname"]))
        db_connection.commit()
        """Check for any tasks for that hostname"""
        sql = """ SELECT * FROM tasks where hostname = %s """
        cur.execute(sql, (data["hostname"],))
        new_task = cur.fetchone()
        if new_task:
            return json.dumps(new_task)
        elif not new_task:
            return json.dumps({"error": "success"})
        cur.close()
        db_connection.close()

    elif not res:  # perhaps it is a new host :)
        sql = """ INSERT INTO hosts(hostname, date, os, architecture, account, external_ip) VALUES (%s, %s, %s, %s, %s, %s) """
        cur.execute(sql, (data["hostname"],
                          data["date"],
                          data["os"],
                          data["architecture"],
                          data["account"],
                          data["external_ip"]))
        db_connection.commit()
        cur.close()
        db_connection.close()
        return json.dumps({"error": "added"})
    else:
        return json.dumps({"error": "failure"})


def updateUpload(data, taskId):
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    sql = """UPDATE output SET status = %s WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, ("Y", taskId, data['action'], data['secondary']))
    db_connection.commit()
    sql = """DELETE FROM tasks WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, (taskId, data['action'], data['secondary']))
    db_connection.commit()
    cur.close()
    db_connection.close()
    return


def updateCommand(data, taskId):
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    # update stdout in output
    sql = """UPDATE output SET stdout = %s WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, (base64.b64decode(data['stdout']).decode(), taskId, data['action'], data['secondary']))
    db_connection.commit()
    # update stderr in output
    sql = """UPDATE output SET stderr = %s WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, (base64.b64decode(data['stderr']).decode(), taskId, data['action'], data['secondary']))
    db_connection.commit()
    # change status
    sql = """UPDATE output SET status = %s WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, ("Y", taskId, data['action'], data['secondary']))
    db_connection.commit()
    # delete task as it has completed
    sql = """DELETE FROM tasks WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, (taskId, data['action'], data['secondary']))
    db_connection.commit()
    cur.close()
    db_connection.close()
    return


def updateDownload(data, taskId):
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    # get the hostname
    sql = """SELECT hostname FROM tasks WHERE id = %s"""
    cur.execute(sql, taskId)
    # change status
    sql = """UPDATE output SET status = %s WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, ("Y", taskId, data['action'], data['secondary']))
    db_connection.commit()
    # delete task as it has completed
    sql = """DELETE FROM tasks WHERE id = %s AND action = %s AND secondary = %s"""
    cur.execute(sql, (taskId, data['action'], data['secondary']))
    db_connection.commit()
    cur.close()
    db_connection.close()
    return


def getHosts():
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("SELECT * from hosts;")
    data = json.dumps(cur.fetchall())
    cur.close()
    db_connection.close()
    return data


def getTasks():
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("SELECT * from tasks;")
    data = json.dumps(cur.fetchall())
    cur.close()
    db_connection.close()
    return data


def delete_task(task_id):
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    sql = """DELETE FROM tasks where id = %s;"""
    cur.execute(sql, (task_id,))
    db_connection.commit()
    cur.close()
    db_connection.close()


def getOutput():
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("SELECT * from output;")
    data = json.dumps(cur.fetchall())
    cur.close()
    db_connection.close()
    return data


def getCommandOutput(output_id, output_type):
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    sql = """SELECT %s from output where id = %s ;"""
    cur.execute(sql, (output_type, output_id))
    data = json.dumps(cur.fetchone())
    cur.close()
    db_connection.close()
    return data


def getHostnames():
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("SELECT hostname from hosts;")
    data = json.dumps(cur.fetchall())
    cur.close()
    db_connection.close()
    return data


def putCommandTask(username, hostname, action, secondary):  # rename to putTask
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    sql = """
    INSERT INTO tasks (username, hostname, action, secondary) values ( '%s','%s', '%s', '%s')
    """
    cur.execute(sql, (username, hostname, action, secondary))
    db_connection.commit()
    cur.close()
    db_connection.close()


def putCommandOutput(username, hostname, action, secondary):  # rename to putOutput
    db_connection = psycopg2.connect(host=conn["host"],
                                     user=conn["username"],
                                     database=conn["database"],
                                     port=conn["port"],
                                     password=conn["password"])
    cur = db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    sql = """
        INSERT INTO output(username, hostname, action, secondary, status) values ( '%s','%s', '%s', '%s', 'N')
        """
    cur.execute(sql, (username, hostname, action, secondary))
    db_connection.commit()
    cur.close()
    db_connection.close()

