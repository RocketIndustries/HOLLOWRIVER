from flask.cli import FlaskGroup
from hollowriver import db, create_app
from hollowriver.models import Users
from werkzeug.security import generate_password_hash

app = create_app()
cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    db.session.add(Users(username="admin", password=generate_password_hash("admin", method='scrypt')))
    db.session.commit()


if __name__ == "__main__":
    cli()
