# HOLLOWRIVER

## Intro

A simple C2.

## Feature overview

*   [x] Postgres, Flask, Gunicorn, Nginx
*   [x] Simple Implant written in C#
*   [ ] Protocol Documentation
*   [x] Implant performs Read, Write, Execution

## Contents

*   [Getting Started](#getting-started)
    *  [Configuration](#configuration)
       * [.env Setup](#env-setup)
       * [Configure HTTPS](#configure-https)
       * [Additional Nginx Configuration](#additional-nginx-configuration)
    *  [Installation](#installation)
    *  [Usage](#usage)
*   [Acknowledgements](#acknowledgements)
*   [License](#license)

## Getting Started

### Configuration

While there is an initial .env file it is seeded with very not so secure values. 

#### .env Setup
The following values in the .env file should be updated: 

*  [ ] POSTGRES_PASSWORD
*  [ ] DATABASE_URL
*  [ ] SECRET_KEY

Finally, if you do not set FLASK_DEBUG to 0, everytime you rebuild it will rebuild the database.
This is defined in `services/web/entrypoint.sh:14`

#### Configure HTTPS

While you can configure SSL however you like, the included certificate/key may fail to be recognized by the implant, now or in a future build, as they are self-signed.
There is really no reason to fore-go the use of Let's Encrypt. Donate money to them if you like their service. 

##### Self-Signed 
This should work to generate certificates for testing.... 
```bash
cd HOLLOWRIVER/services/nginx
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout server.key -out server.crt
```

##### Let's Encrypt 
Run the below command and follow the prompts substituting your domain for `example.com` to collect your certificates.
```bash
certbot certonly --manual --preferred-challenges=dns --email admin@example.com --server https://acme-v02.api.letsencrypt.org/directory --agree-tos -d '*.example.com' -d 'example.com' --force-interactive
```
Copy the server certificate and server key to `services/nginx/` and ensure that `services/nginx/Dockerfile` reflects the names of the newly created certificates. For the sake of simplicity these have been created as `server.crt` and `server.key`

#### Additional Nginx Configuration
Line 8 in `services/nginx/nginx-ssl.conf` should be updated to reflect your server name. 

### Installation

To start the server:
```bash
git clone https://github.com/#####
cd #####
docker-compose up -d 
```

To build the implant:
```powershell
cd <directory of implant>
msbuild
```

### Usage

Initial username is `admin` and password is `admin`.

## TODO

What's not todo? 

* [ ] Fix the External IP address issue with nginx
* [ ] Have implant generate UUID for reference instead of hostname.
* [ ] Rename some functions to be clearer.
* [ ] Typing and Documentation
* [ ] Add additional functionality for implant including:
  * [ ] Persistence
  * [ ] DLL loading
  * [ ] Self-Destruct
  * [ ] Common Collection Automation
  * [ ] Encryption
* [ ] GeoIP integration
* [ ] Implant in c++, golang, python
* [ ] Documentation Section in the Commands Tasking
* [ ] Commands scripting engine


## Acknowledgements

[CrunchRAT][CrunchRAT] - Project which served as inspiration. Years ago, at B-Sides Augusta. Props for the inspiration. 

[//]: # "Source Definitions"
[CrunchRAT]: https://github.com/erikbarzdukas/CrunchRAT "crunchRAT" 

## License
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text)
